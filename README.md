## Multidimensional Integration in C++

`cubacpp` provides a C++ binding for the excellent [CUBA](http://www.feynarts.de/cuba)
library. `cubacpp` provides no new integration facilities. It merely provides a
more convenient syntax for use of the CUBA integration routines, made possible
by the features of the C++ programming language.

## Obtaining and building the software

### Obtaining cubacpp

Please clone the repository! If you wish to contribute, fork the repository, and
send me pull requests.

### Obtaining  and building CUBA

To use `cubacpp`, you must have a copy of the CUBA software, available from
<http://www.feynarts.de/cuba>. The directory `scripts` contains a script for
downloading and building CUBA into a dynamic library on a Linux system. This is
the way CUBA is used in the projects in which I participate. You may need to
adjust the variable settings in the script. Building CUBA requires only a
C compiler, compliant with the C89 standard.

### Building cubacpp

There is nothing you need to build for `cubacpp` itself; the library is header-only. To
use `cubacpp`, you will need a compiler that support C++17. GCC v7 meets the
need, when run with the flag `-std=c++17`. Clang v5 meets the need, also when
run with flag `-std=c++17`. Apple Clang as of version 9.2, when run with `-std=c++1z`,
also meets the need. I would be happy to receive reports of other 
compilers that also suffice.

The `test` directory contains an example showing how to use the integration
functions provided by `cubacpp`. It also contains a CMakeLists.txt file to
be used by CMake, to create a build. You may need to direct CMake to the
compiler you wish to use. Most likely, you will also have to direct CMake
to the location of your CUBA installation.
For example, on a machine with GCC 7.x installed
as `g++-7` (which can be obtained through Homebrew on macOS), and with
CUBA installed under $HOME, the following command would create a `Makefile`
to build the testing programs.

```bash
$ cmake -DCUBA_DIR=$HOME -DCMAKE_CXX_COMPILER=$(which g++-7)
```
## Using cubacpp

The user interface of `cubacpp` consists of a few function templates,
and a few structs,
one of each for
each of the integration routines supplied by CUBA. 

The simplest use of `cubacpp` is through the function templates.
Each of the function
templates takes, as its first argument, the function to be integrated. This
integrand can be supplied as any C++ callable object:

1. A free function (e.g., a C-style function).
2. An object of a class that supplies a function call operator (`operator()`),
   including an instance of `std::function`.
3. A lambda expression.

In each case, the supplied callable object must be callable with zero or more
arguments of type `double`; thus the function arguments must either be `double`,
or some other numeric type convertible to `double`. CUBA can calculate vector-valued
integrals; to take advantage of this facility, the callable object is allowed to
return either (one) `double`, or an `std::array<double, N>`, for any positive integral
value of `N`.

### Using the function templates

The detailed description of the CUBA integration routines are available
at the [CUBA web site](http://www.feynarts.de/cuba). We describe here how the arguments of
the CUBA functions are determined from the `cubacpp` call.

#### Common arguments

These arguments are common to all the CUBA integration routines. Note that
the `ll` versions (those using `long long int` arguments) 
 of the CUBA routines are used by `cubacpp`, so several
of the arguments that are listed in the CUBA documentation as being of
type `int` are in fact of type `long long int`, as described in section 7.4
of the CUBA documentation.

1. `int ndim`: This value is determined automatically from the
    number of arguments required to call the user-supplied
    integrand. Note that the Cuhre algorithm requires an integrand
    which takes at least two arguments; trying to use it with
    a function of fewer arguments will produce a compilation
    failure in `cubacpp`, rather then the runtime error of
    CUBA.
2. `int ncomp`: The value is determined automatically from the
   number of `double` values returned by a call to the user-supplied
   integrand. 
3. `integrand`: This function is automatically generated based
   on the user-supplied integrand, supplied as any callable C++
   object (e.g. a free function, an instance of a class with
   a function call operator, or a lambda expression).
4. `void* userdata`: This value passed is a pointer to the
   function or other callable object passed as the integrand.
5. `int nvec`: The value is set to 1 by `cubacpp`.
6. `double epsrel, epsabs`: The values provided to the
   `epsrel` and `epsabs` arguments of the `cubacpp` function
   are passed to CUBA.
7. `int flags`: The value provided by the `flags` argument
   of the `cubacpp` functions is passed to CUBA.
8. `int seed`: The value is set to 0 by `cubacpp`.
9. `long long mineval`: The value supplied by the
   user is passed to CUBA. A default of 0 is provided
   by `cubacpp`.
10. `long long int maxeval`: The value supplied by the user
    is passed to CUBA. A default of 50,000 is provided
    by `cubacpp`.
11. `const char* statefile`: The value `nullptr` is supplied
    to CUBA. Using a stored state file seems problematic in an
    MPI environment.
12. `void* spin`: The value `nullptr` is passed to CUBA. Using
    CUBA to manage multiple operating system processes is
    problematic in an MPI environment.
13. `long long int neval`: The value returned by CUBA in this
    output argument is returned in the `integration_result`
    struct returned by all the `cubacpp` integration functions,
    in the data member `neval`.
14. `int fail`: The value returned by CUBA in this output
    argument is returned in the `integration_result` struct
    returned by all the `cubacpp` integration functions, in
    the data member `status`.
15. `double integral`: The value returned by CUBA in this output
    argument is returned in the `integration_result` struct
    returned by all the `cubacpp` integration functions, in the
    data member `value`.
16. `double error`: The value returned by CUBA in this output
    argument is returned in the `integration_result` struct
    returned by all the `cubacpp` integration functions, in the
    data member `error`.
17. `double prob`: The value returned by CUBA in this output
    argument is returned in the `integration_result` struct
    returned by all the `cubacpp` integration functions, in the
    data member `prob`.

#### Vegas-specific arguments

1. `long long int nstart`: The value supplied by the user is passed
   to Vegas. A default of 1000 is supplied by `cubacpp`.
2. `long long int nincrease`: The value supplied by the user is passed
   to Vegas. A default of 500 is supplied by `cubacpp`.
3. `long long int nbatch`: The value supplied by the user is passed
   to Vegas. A default of 1000 is supplied by `cubacpp`.
4. `integer gridno`: The value 0 passed to Vegas.

#### Suave-specific arguments

1. `int* nregions`: The returned value is not available through the `cubacpp`
   interface.
2. `long long int nnew`: The value supplied by the user is passed
   to Suave. A default of 1000 is supplied by `cubacpp`.
3. `long long int nmin`: The value supplied by the user is passed
   to Suave. A default of 2 is provided by `cubacpp`.
4. `double flatness`: The value supplied by the user is passed to
   Suave. A default of 25.0 is provided by `cubacpp`.

#### Cuhre-specific arguments

1. `int* nregions`: The returned value is not available through the
   `cubacpp` interface.
2. `int key`: The value supplied by the user is passed to Cuhre.
   A default value of -1 is supplied by `cubacpp`. If -1 is passed,
   `cubacpp` sets the degree of the rule to the maximum allowed,
   based on the dimension of the integrand.

#### Divonne-specific arguments

An interface to the Divonne algorithm is not yet implemented.

### Using the integration structs

Because there are so many function parameters that may be passed to each of the
function templates, `cubacpp` also contains a struct for each integration
algorithm, each of which contains a data member for each parameter that controls
the integral evaluation, except for `epsrel` and `epsabs`. The individual
parameters may be set by assigning to the relevant data members. The integration
may then be done by calling the `integrate` member function of the struct,
passing the function to be integrated, and the values of `epsrel` and `epsabs`.

## Issues with parallelism

Because the main use case for `cubacpp` is in an MPI environment, use of CUBA's
facilities for forking multiple processes to speed calculations is problematic.
I recommend that users call `cubacores(0, 0)` before executing any `cubacpp`
 
## Special notes for users of CosmoSIS

The `scripts` directory contains a `Dockerfile` suitable for use with the
_comsosis-docker_ delivery of CosmoSIS. Please make sure your version of
_cosmosis-docker_ is sufficiently recent; this means the version of the
GCC compiler is v7 or newer.
