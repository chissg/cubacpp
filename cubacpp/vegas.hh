#ifndef CUBACPP_VEGAS_HH
#define CUBACPP_VEGAS_HH

#include "cuba.h"
#include "cubacpp/arity.hh"
#include "cubacpp/integrand.hh"
#include "cubacpp/integrand_traits.hh"
#include "cubacpp/integration_result.hh"

namespace cubacpp {

  template <class F>
  integration_results<detail::integrand_traits<F>::ncomp>
  VegasIntegrate(F const& f,
                 double epsrel,
                 double epsabs,
                 int flags = 0,
                 long long mineval = 0,
                 long long maxeval = 50000,
                 long long nstart = 1000,
                 long long nincrease = 500,
                 long long nbatch = 1000)
  {
    integrand_t igrand = detail::integrand<F>;
    constexpr int nvec = 1;
    long long neval = 0;
    int fail = 0;
    using result_type = typename detail::integrand_traits<F>::result_type;
    result_type val{};
    result_type err{};
    result_type prob{};
    llVegas(detail::integrand_traits<F>::ndim,
            detail::integrand_traits<F>::ncomp,
            igrand,
            (void*)&f,
            nvec,
            epsrel,
            epsabs,
            flags,
            0, // seed
            mineval,
            maxeval,
            nstart,
            nincrease,
            nbatch,
            0,
            nullptr,
            nullptr,
            &neval,
            &fail,
            (double*)&val,
            (double*)&err,
            (double*)&prob);
    return {val, err, prob, neval, fail};
  }

  struct Vegas {
    int flags = 0;
    long long int mineval = 0;
    long long int maxeval = 50000;
    long long nstart = 1000;
    long long nincrease = 500;
    long long nbatch = 1000;

    template <class F>
    integration_results<detail::integrand_traits<F>::ncomp>
    integrate(F const& f, double epsrel, double epsabs) const
    {
      return VegasIntegrate(
        f, epsrel, epsabs, flags, mineval, maxeval, nstart, nincrease, nbatch);
    }
  };
}

#endif