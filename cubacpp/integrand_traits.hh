#ifndef CUBACPP_INTEGRAND_TRAITS_HH
#define CUBACPP_INTEGRAND_TRAITS_HH

#include "cubacpp/arity.hh"
#include <array>
#include <tuple> // for std::apply

namespace cubacpp {

  namespace detail {
    // The function template integrand is to be used, in conjunction with a
    // user-supplied class F, to provide the C-function to be passed to one of
    // the CUBA library's integration routines.
    template <class F>
    int integrand(int const*, double const*, int const*, double* f, void*);

    // integrand_traits<F> provides information about the callable type
    // F. The static data member 'ndim' reports the number of arguments
    // of the callable object (the dimensionality of the integral to be
    // calculated). The nested type 'result_type' reports the return type
    // of the call. Expected types are 'double', or 'std::array<double, N>'
    // for any positive integral N.
    template <class F>
    struct integrand_traits {
      static constexpr auto ndim = cubacpp::util::arity<F>();
      using result_type =
        decltype(std::apply(std::declval<F>(), std::array<double, ndim>()));
      static constexpr std::size_t ncomp = sizeof(result_type) / sizeof(double);
    };
  }
}
#endif
