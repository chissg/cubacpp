#ifndef CUBACPP_ARITY_HH
#define CUBACPP_ARITY_HH

#include <type_traits>

// clang++, as of v 5.0.1, does not yet have std::invocable. However, it does
// have std::__invokable, that does the job. So we hack this...

#ifdef __clang__
#if (__clang_major__ < 6)
namespace std {
  template <class F, class... ARGS>
  using is_invocable = __invokable<F, ARGS...>;
}
#endif
#endif

namespace cubacpp::util {

  template <typename F, typename ArgType, int Limit, typename... Args>
  constexpr int
  nargs_with_type_impl()
  {
    if constexpr (sizeof...(Args) == Limit) {
      return -1;
    } else if (std::is_invocable<F, Args...>::value) {
      return sizeof...(Args);
    } else {
      return nargs_with_type_impl<F,
                                  ArgType,
                                  Limit,
                                  ArgType,
                                  Args...>(); // Add another argument
    }
  }

  template <typename F, typename T, int Limit = 30>
  constexpr int
  nargs_with_type()
  {
    return nargs_with_type_impl<F, T, Limit + 1>();
  }

  template <typename F>
  constexpr int arity(F)
  {
    return nargs_with_type<F, double>();
  }

  template <typename F>
  constexpr int
  arity()
  {
    return nargs_with_type<F, double>();
  }
}

#endif
