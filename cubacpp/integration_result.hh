#ifndef CUBACPP_INTEGRATION_RESULT_HH
#define CUBACPP_INTEGRATION_RESULT_HH

#include <ostream>

namespace cubacpp {

  // All integration functions in cubacpp return an object of type
  // integration_result. This object contains the output of the
  // underlying CUBA integration routine called.
  template <std::size_t N>
  struct integration_results {
    std::array<double, N> value;
    std::array<double, N> error;
    std::array<double, N> prob;
    long long neval;
    int status;

    bool
    converged() const
    {
      return status == 0;
    };
  };

  template <>
  struct integration_results<1> {
    double value;    // the best estimate of the integral
    double error;    // the estimated uncertainty of the integral
    double prob;     // the chisquared probability; see CUBA docs
    long long neval; // the number of integrand evalutions used
    int status;      // 0 indicates convergence, anything else failure

    bool
    converged() const
    {
      return status == 0;
    };
  };

  using integration_result = integration_results<1>;

  template <std::size_t N>
  std::ostream&
  operator<<(std::ostream& os, integration_results<N> const& r)
  {
    for (std::size_t i = 0; i != N; ++i) {
      os << "Value: " << r.value[i] << " +/- " << r.error[i]
         << " prob: " << r.prob[i] << '\n';
    }
    os << " neval: " << r.neval << " status: " << r.status;
    return os;
  }

  std::ostream&
  operator<<(std::ostream& os, integration_result const& r)
  {
    os << "Value: " << r.value << " +/- " << r.error << " prob: " << r.prob
       << " neval: " << r.neval << " status: " << r.status;
    return os;
  }
}

#endif
