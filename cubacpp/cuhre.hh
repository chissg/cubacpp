#ifndef CUBACPP_CUHRE_HH
#define CUBACPP_CUHRE_HH

#include "cuba.h"
#include "cubacpp/arity.hh"
#include "cubacpp/integrand.hh"
#include "cubacpp/integrand_traits.hh"
#include "cubacpp/integration_result.hh"

namespace cubacpp {

  template <class F>
  integration_results<detail::integrand_traits<F>::ncomp>
  CuhreIntegrate(F const& f,
                 double epsrel,
                 double epsabs,
                 int flags = 0,
                 long long mineval = 0,
                 long long maxeval = 50000,
                 int key = -1)
  {
    // If key was not specified, deduce the highest order we can use based on
    // the dimensionality of the integrand.
    constexpr auto N = detail::integrand_traits<F>::ndim;
    static_assert(N >= 2,
                  "Cuhre requires 2 or more dimension in the integrand");
    if (key < 0) {
      switch (N) {
        case 1:
        case 2:
          key = 13;
          break;
        case 3:
          key = 11;
          break;
        default:
          key = 9;
      }
    }
    if (key != 7 && key != 9 && key != 11 && key != 13) {
      integration_results<detail::integrand_traits<F>::ncomp> res{};
      res.status = -2;
      return res;
    }
    integrand_t igrand = detail::integrand<F>;
    constexpr int nvec = 1;
    int nregions = 0;
    long long neval = 0;
    int fail = 0;
    using result_type = typename detail::integrand_traits<F>::result_type;
    result_type val{};
    result_type err{};
    result_type prob{};
    llCuhre(detail::integrand_traits<F>::ndim,
            detail::integrand_traits<F>::ncomp,
            igrand,
            (void*)&f,
            nvec,
            epsrel,
            epsabs,
            flags,
            mineval,
            maxeval,
            key,
            nullptr,
            nullptr,
            &nregions,
            &neval,
            &fail,
            (double*)&val,
            (double*)&err,
            (double*)&prob);
    return {val, err, prob, neval, fail};
  }

  struct Cuhre {
    int flags = 0;
    long long int mineval = 0;
    long long int maxeval = 50000;
    int key = -1;

    template <class F>
    integration_results<detail::integrand_traits<F>::ncomp>
    integrate(F const& f, double epsrel, double epsabs) const
    {
      return CuhreIntegrate(f, epsrel, epsabs, flags, mineval, maxeval, key);
    }
  };
}

#endif
