#ifndef CUBACPP_INTEGRAND_HH
#define CUBACPP_INTEGRAND_HH

#include "cubacpp/integrand_traits.hh"
#include <array>
#include <cstddef>
#include <tuple> // for std::apply

namespace cubacpp::detail {

  template <std::size_t N>
  std::array<double, N>
  to_array(double const* x)
  {
    std::array<double, N> res;
    for (std::size_t i = 0; i < N; ++i)
      res[i] = x[i];
    return res;
  }

  template <std::size_t N>
  std::array<double, N>
  to_array(std::array<double, N> const& x)
  {
    return x;
  }

  inline std::array<double, 1>
  to_array(double x)
  {
    // The doubled braces here are used to keep clang++ quiet.
    return {{x}};
  }

  template <class F>
  int
  integrand(int const* ndim,
            double const x[],
            int const* /* ncomp */,
            double f[],
            void* obj)
  {
    constexpr auto N = integrand_traits<F>::ndim;
    if (*ndim != N)
      return -999;
    F const* fcn = reinterpret_cast<F const*>(obj);
    auto res = to_array(std::apply(*fcn, to_array<N>(x)));
    for (auto& val : res) {
      *f = val;
      ++f;
    }
    return 0;
  }
}

#endif
